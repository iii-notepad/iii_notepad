;(function ($, window, document, undefined) {
    'use strict';

    var url         = 'https://notepad.iktutor.com:3000';
    var plugin_url  = iii_script.plugin_url;
    var stanza      = iii_script.roomid;

    var controlpencil = true;
    var controlrubber = false;
    var positionx = '0';
    var positiony = '0';
    var lastEmit = $.now();
    var touchX, touchY;

    var divrubber = $('#divrubber');

    // A flag for drawing activity
    var drawing = false;
    var cursors = {};
    var dragging = false;

    //  funzione richiesta di nick name
    var username;

    var id = username = Math.round($.now() * Math.random());
    if (!username) {
        username = prompt("Hey there, insert your nick name, please", "");
    }
    var totalSeconds = 0;
    var idtempo;

    var click_event = document.ontouchstart ? 'touchstart' : 'click';

    var webSyncHandleUrl = 'http://websync.iktutor.com:8080/websync.ashx';
    fm.websync.client.enableMultiple = true;
    var clients = new fm.websync.client(webSyncHandleUrl);
    var testichat = document.getElementById('testichat');
    var cnt = 0;

    $(function() {
        var $task_wrap  = $('.taskbar-notepad');

        $task_wrap.find('.tool-btn').on('click', function() {
            var $this = $(this);

            $('.tooltip-wrap').removeClass('show-tt');

            var cnt     = getCanvas();
            $('#panel').find('.typing-input').remove();
            $('#add-type-box').removeClass('active');
            $('#math' + cnt).unbind(click_event, TypingCreatTextarea);
            $('#math' + cnt)[0].removeEventListener('touchstart', TypingCreatTextareaTouchDevice);

            if ($this.hasClass('active')) {
                $this.removeClass('active');
                $this.next('.tool-submenu').addClass('hidden');
            } else {
                $task_wrap.find('.tool-btn').removeClass('active');
                $task_wrap.find('.tool-submenu').addClass('hidden');

                $this.addClass('active');
                $this.next('.tool-submenu').removeClass('hidden');
            }
        });

        $('#change-eraser').on('click', function () {
            $('#divrubber').css({ "display": "block", "width": "30px", "height": "30px", "visibility": "visible" });
            $('#controlrubber').addClass('css-cursor-30');
            $("#erasers-body").find(`[data-eraser='30']`).addClass('active');

            canEraser();
        });

        $('#change-size-pencil').on('click', function () {
            var cnt = getCanvas();

            $("#erasers-body li").removeClass('active');
            $('#divrubber').css("display", "none");
            canDraw();

            if (parseInt($(this).attr('data-layer')) === (cnt - 3)) {
                return;
            }

            $(this).attr('data-layer', cnt - 3);
        });

        //display screenshots
        $("#icon-screenshot").click(function() {
            var check = $('.screenshot-class').hasClass("hidden");
            if (check == true) {
                $('.screenshot-class').removeClass("hidden");
            } else {
                $('.screenshot-class').addClass("hidden");
            }
        });

        // onscreen typing tool
        $("#add-type-box").on(click_event, function () {
            var cnt     = getCanvas();
            var next    = (parseInt(cnt) + 1);

            $('#math' + cnt).unbind(click_event, TypingCreatTextarea);
            $('#math' + cnt)[0].removeEventListener('touchstart', TypingCreatTextareaTouchDevice);

            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
                $('#math' + cnt).bind(click_event, TypingCreatTextarea);
                $('#math' + cnt)[0].addEventListener('touchstart', TypingCreatTextareaTouchDevice);
            } else {
                $(this).removeClass('active');
                $('#math' + cnt).unbind(click_event, TypingCreatTextarea);
                $('#math' + cnt)[0].removeEventListener('touchstart', TypingCreatTextareaTouchDevice);
                $('#panel').find('.typing-input').remove();
            }
        });

        $('body').on(click_event, function() {
            var cnt = getCanvas();
            var can = $('#math' + cnt);
            var ctxcan = can[0].getContext('2d');

            $('#panel').find('.typing-input').each(function () {
               var $this        = $(this),
                    position    = $this.position(),
                    left        = position.left,
                    top         = position.top + 10;

                var arrLine = $this.val().split('\n');
                var $i = 1;
                $.each(arrLine, function(index, item) {
                    var item_top = top + $i * 10;
                    ctxcan.fillText(item, left, item_top);
                    $i++;
                });

                if ($('#panel').find('.typing-input').length > 1) {
                    $this.remove();
                }
            });
        });

        $("select").selectBoxIt();

        //event close session
        $("#close-session").on('click', function() {
            $('.close-session').removeClass("hidden");
        });

        $(".close-popup-close").click(function() {
            $('.close-class').addClass("hidden");
        });

        $('#file-input').change(function(e) {
            $('#panel').find('.typing-input').remove();

            var file = e.target.files[0],
                imageType = /image.*/;
            if (!file.type.match(imageType))
                return;

            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);

            var cnt = getCanvas();
            var can = $('#math' + cnt);
            can.addClass('has-image');
        });

        $('#divrubber').draggable();

        $('.full-screen-mode').on('click', function () {
            let $this = $(this);

            if ($this.hasClass('full')) {
                $this.removeClass('full').addClass('min');
                openFullscreen();
            } else if ($this.hasClass('min')) {
                $this.removeClass('min').addClass('full');
                closeFullscreen();
            }

        });

        createCanvas();
        initDraw();
        addLayer();
        deleteLayer();
        clearState();
        activeTab();
        hoverMenuShowTooltip();
    });

        var timeout; 
         $('.tooltip-wrap').on('mouseenter', function () {
            var $this = $(this);

            if(timeout != null) { clearTimeout(timeout);}

            timeout = setTimeout(function () {
                 $('.tooltip-wrap').removeClass('show-tt');
                 $this.addClass('show-tt');
            }, 1000)
        });

        $('.tooltip-wrap').on('mouseleave', function(){
            if(timeout != null){
                clearTimeout(timeout);
                timeout = null;
            }
        });
        $('.tooltip-wrap').on('mouseover', function(){
            $('.tooltip-wrap').removeClass('show-tt');
        });


    function openFullscreen() {
        let elem = document.documentElement;

        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) { /* Firefox */
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { /* IE/Edge */
            elem.msRequestFullscreen();
        }
    }

    function closeFullscreen() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }

    function TypingCreatTextarea(e) {
        e.preventDefault();

        var textarea = $('<textarea/>', {
            class: 'typing-input'
        }).appendTo('#panel');

        var top = e.clientY - 70;

        textarea.css({
            top: top + 'px',
            left: e.clientX + 'px'
        });
    }

    function TypingCreatTextareaTouchDevice(e) {
        e.preventDefault();

        var textarea = $('<textarea/>', {
            class: 'typing-input'
        }).appendTo('#panel');

        if (e.touches) {
            if (e.touches.length == 1) {
                var touch = e.touches[0];

                var top = touch.pageY - 70;

                textarea.css({
                    top: top + 'px',
                    left: touch.pageX + 'px'
                });
            }
        }
    }

    function createCanvas() {
        $("#layers-body li").each(function(i) {
            $(this).addClass("item" + (i + 1));
            $(this).attr("data-cnt", (i + 1));
            $(this).find('span').text((i + 1));

            if ($(this).hasClass('active')) {
                var canvas = cloneCanvas((i + 1), true);

                $('#panel').append(canvas);
                canDraw();
            } else {
                var canvas = cloneCanvas((i + 1));
                $('#panel').append(canvas);
            }
        });
    }

    function initDraw() {
        $(document).on('click', '.icon-selector', function() {
            var cnt = $(this).attr('data-cnt');

            $('#panel').find('.typing-input').remove();
            $('#add-type-box').removeClass('active');
            $('#math' + cnt).unbind(click_event, TypingCreatTextarea);
            $('#math' + cnt)[0].removeEventListener('touchstart', TypingCreatTextareaTouchDevice);

            if (!$(this).hasClass('active') && !$(this).hasClass('none-visiable')) {
                $("#layers-body li").removeClass('active');
                $(this).addClass('active');
                $('#divrubber').css("visibility", "hidden");
                $("#erasers-body li").removeClass('active');

                canDraw();

                $('#yotubeVideo .item-video').addClass('hidden');
                $('#yotubeVideo #video-' + cnt).removeClass('hidden');

                drawVideo();

                $('canvas').removeClass('selected');
                $('#math' + cnt).addClass('active selected');
                $('#math' + cnt).css('visibility', 'visible');
                //$('#math' + cnt).css('z-index', 10);

                if ($('.icon-layer.btn-grid').hasClass('active')) {
                    var grid    = $('.icon-layer.btn-grid.active').attr('data-grid');
                    var cnv     = $('#math' + cnt);
                    var ctxcan  = cnv[0].getContext('2d');


                    var bg = getBg();
                    if (bg != '') {
                        $('#panel').css('background-color', bg);
                    }

                    if (grid) {
                        $('#panel').css('background-image', 'url('+ plugin_url + '/assets/images/notepad/grid/grid-' + grid + '.png)')
                    }
                }
            } else if ($(this).hasClass('active')) {
                $(this).removeClass('active').addClass('none-visiable');
                $(this).find('img').attr('src', plugin_url + '/assets/images/notepad/layer/Layer_' + (cnt - 3) + '_Not_Visible.png');

                $('#math' + cnt).css('visibility', 'hidden');
                $('#math' + cnt).removeClass('active').addClass('no-visiable');
                //$('#math' + cnt).css('z-index', cnt);
            } else if ($(this).hasClass('none-visiable')) {
                $("#layers-body li").removeClass('active');
                $(this).find('img').attr('src', plugin_url + 'assets/images/notepad/layer/layer-' + (cnt - 3) + '.png');
                $(this).removeClass('none-visiable').addClass('active');

                $('#math' + cnt).css('visibility', 'visible');
                $('#math' + cnt).removeClass('no-visiable').addClass('active');
                //$('#math' + cnt).css('z-index', cnt);
            }
        });

        $(document).on('click', '.btn-color', function() {
            var cnt     = getCanvas();
            $('#panel').find('.typing-input').remove();
            $('#add-type-box').removeClass('active');
            $('#math' + cnt).unbind(click_event, TypingCreatTextarea);
            $('#math' + cnt)[0].removeEventListener('touchstart', TypingCreatTextareaTouchDevice);

            if (!$(this).hasClass('active')) {
                $("#colors-body li").removeClass('active');
                $(this).addClass('active');
                $('#divrubber').css("display", "none");
                $("#erasers-body li").removeClass('active');
                //$("#pencils-body li.hr1").addClass('active');
                $('#change-color img').attr('src', plugin_url + '/assets/images/notepad/color/top/' + $(this).attr('data-image-url'));
                canDraw();
            }
        });

        $(document).on('click', '.btn-pencil', function() {
            var cnt     = getCanvas();
            $('#panel').find('.typing-input').remove();
            $('#add-type-box').removeClass('active');
            $('#math' + cnt).unbind(click_event, TypingCreatTextarea);
            $('#math' + cnt)[0].removeEventListener('touchstart', TypingCreatTextareaTouchDevice);

            if (!$(this).hasClass('active')) {
                $("#pencils-body li").removeClass('active');
                $("#erasers-body li").removeClass('active');
                $(this).addClass('active');
                $('#divrubber').css("display", "none");
                canDraw();
            }
        });

        $('.btn-eraser').on('click', function () {
            var cnt     = getCanvas();
            $('#panel').find('.typing-input').remove();
            $('#add-type-box').removeClass('active');
            $('#math' + cnt).unbind(click_event, TypingCreatTextarea);
            $('#math' + cnt)[0].removeEventListener('touchstart', TypingCreatTextareaTouchDevice);

            var $this = $(this);

            $("#erasers-body li").removeClass('active');
            //$("#pencils-body li").removeClass('active');
            $this.addClass('active');

            var val = $this.attr('data-eraser');

            $('#divrubber').css({ "display": "block", "width": val + "px", "height": val + "px" });
            $('#controlrubber').removeClass('css-cursor-30 css-cursor-50 css-cursor-70 css-cursor-90 css-cursor-100');
            $('#controlrubber').addClass('css-cursor-' + val);

            canEraser();
        });

        var $z;
        $(document).on('click', '.btn-color-grid', function() {
            var bg      = $(this).attr('data-color');
            var cnt     = getCanvas();
            var can     = $('#math' + cnt);
            var ctxcan  = can[0].getContext('2d');

            $('#panel').find('.typing-input').remove();
            $('#add-type-box').removeClass('active');
            $('#math' + cnt).unbind(click_event, TypingCreatTextarea);
            $('#math' + cnt)[0].removeEventListener('touchstart', TypingCreatTextareaTouchDevice);

            var $x      = $("#grids-body .btn-color-grid.active").length;

            if (!$(this).hasClass('active')) {
                $("#grids-body .btn-color-grid").removeClass('active');
                $(this).addClass('active');

                if ($x === 0) {
                    //
                }
                $('#panel').css('background-color', bg);

                var grid = getGrid();
                if (grid != '') {
                   $('#panel').css('background-image', 'url('+ plugin_url + '/assets/images/notepad/grid/grid-' + grid + '.png)');
                }
            }
        });

        var $stepGrid = -1;

        $(document).on('click', '.btn-grid', function() {
            var cnt = getCanvas();
            var cnv = $('#math' + cnt);
            var ctxcan = cnv[0].getContext('2d');

            $('#panel').find('.typing-input').remove();
            $('#add-type-box').removeClass('active');
            $('#math' + cnt).unbind(click_event, TypingCreatTextarea);
            $('#math' + cnt)[0].removeEventListener('touchstart', TypingCreatTextareaTouchDevice);

            var bg = getBg();
            if (bg != '') {
                $('#panel').css('background-color', bg);
            }

            if (!$(this).hasClass('active')) {
                $("#grids-body .btn-grid").removeClass('active');
                $(this).addClass('active');

                var grid = $(this).attr('data-grid');
                // ctxcan.globalCompositeOperation = "destination-over";
                // drawGrid(cnv, grid, bg);
                $('#panel').css('background-image', 'url('+ plugin_url + '/assets/images/notepad/grid/grid-' + grid + '.png)');
                //$('canvas').css('background-image', 'url('+ plugin_url + '/assets/images/grid-' + grid + '.png)');
            } else {
                $(this).removeClass('active');
                $('#panel').css('background-image', '');
                //$('canvas').css('background-image', '');
            }
        });


        $(document).on('change','#screenshot-check', function(ev) {
            if (document.getElementById('screenshot-check').checked) {
                $("#select-screenshot").selectBoxIt('disable');
                var val = $('#select-screenshotSelectBoxItText').attr('data-val') * 1000;
                idtempo = setInterval(function() {
                    takepicture();
                }, val);
            }else{
                clearInterval(idtempo);
                $("#select-screenshot").selectBoxIt('enable');
            }
        });
    }

    function takepicture(e) {
        var cnt     = getCanvas();
        var can     = $('#math' + cnt);
        var datacam = can[0].toDataURL('image/png');
        var socket  = io.connect(url);

        socket.emit('camperaltri', {
            'id': id,
            'positionx': positionx,
            'positiony': positiony,
            'camperaltridati': datacam,
            'room': stanza
        });
    }

    function drawVideo() {
        var mediaSource = "http://www.youtube.com/watch?v=nOEw9iiopwI";

        var muted = true;

        var cnt     = getCanvas();
        var can     = $('#math' + cnt);
        var ctx     = can[0].getContext("2d");

        var videoContainer;
        var video = $('<video/>', {
            id: '3itest',
        });

        var source = $('<source/>', {
            src: mediaSource,
            type: 'video/youtube'
        }).appendTo(video);

        //video.autoPlay  = false;
       // video.loop      = true;
        //video.muted     = muted;

        videoContainer = {
             video : video,
             ready : false,
        };

        video.appendTo($('body'));

        video.oncanplay = readyToPlayVideo;

        function readyToPlayVideo(event){
            videoContainer.scale = Math.min(4, 3);
            videoContainer.ready = true;

            requestAnimationFrame(updateCanvas);
        }

        function updateCanvas() {
            ctx.clearRect(0,0,can.width,can.height);

            if(videoContainer !== undefined && videoContainer.ready){
                // find the top left of the video on the canvas
                video.muted = muted;
                var scale = videoContainer.scale;
                var vidH = videoContainer.video.videoHeight;
                var vidW = videoContainer.video.videoWidth;
                var top = 200;
                var left = 300;

                // now just draw the video the correct size
                ctx.drawImage(videoContainer.video, left, top, vidW * scale, vidH * scale);
                if(videoContainer.video.paused){ // if not playing show the paused screen
                    drawPayIcon();
                }
            }

            // all done for display
            // request the next frame in 1/60th of a second
            requestAnimationFrame(updateCanvas);
        }

        function drawPayIcon(){
             ctx.fillStyle = "black";
             ctx.globalAlpha = 0.5;
             ctx.fillRect(0,0,can.width,can.height);
             ctx.fillStyle = "#DDD";
             ctx.globalAlpha = 0.75;
             ctx.beginPath();
             var size = (can.height / 2) * 0.5;
             ctx.moveTo(can.width/2 + size/2, can.height / 2);
             ctx.lineTo(can.width/2 - size/2, can.height / 2 + size);
             ctx.lineTo(can.width/2 - size/2, can.height / 2 - size);
             ctx.closePath();
             ctx.fill();
             ctx.globalAlpha = 1;
        }

        function playPauseClick(){
             if(videoContainer !== undefined && videoContainer.ready){
                  if(videoContainer.video.paused){
                        videoContainer.video.play();
                  }else{
                        videoContainer.video.pause();
                  }
             }
        }

        can[0].addEventListener("click",playPauseClick);
    }

    function drawGridLines(cnv, lineOptions) {
        var iWidth = cnv[0].width;
        var iHeight = cnv[0].height;

        var ctx = cnv[0].getContext('2d');

        ctx.strokeStyle = lineOptions.color;
        ctx.strokeWidth = 1;
        ctx.lineWidth   = 1;

        ctx.beginPath();

        var iCount = null;
        var i = null;
        var x = null;
        var y = null;

        iCount = Math.floor(iWidth / lineOptions.separation);

        for (i = 1; i <= iCount; i++) {
            x = (i * lineOptions.separation);
            ctx.moveTo(x, 0);
            ctx.lineTo(x, iHeight);
            ctx.stroke();
        }

        iCount = Math.floor(iHeight / lineOptions.separation);

        for (i = 1; i <= iCount; i++) {
            y = (i * lineOptions.separation);
            ctx.moveTo(0, y);
            ctx.lineTo(iWidth, y);
            ctx.stroke();
        }

        ctx.closePath();

        return;
    }

    function addLayer() {
        $(".btn-add-layer").click(function() {
            $('#panel').find('.typing-input').remove();
            var len = $("#layers-body li").length;
            if (len <= 8) {
                $('#layers-body').append('<li class="icon-layer icon-selector item' + (len + 1)
                    + '" data-cnt="' + (len + 1) + '"><img src="' + plugin_url + 'assets/images/notepad/layer/layer-' + (len - 2) + '.png"></li>');

                var canvas = cloneCanvas((len + 1));
                $('#panel').append(canvas);

                $('#math' + (len + 1)).css('z-index', (len + 1));

                canDraw();
            }
        });
    }

    function deleteLayer() {
        $(".btn-delete-layer").click(function() {
            var len = $("#layers-body li").length;
            if (len > 4) {
                $("#math" + len).remove();
                $(".item" + len).remove();
            }
        });
    }

    function clearState() {
        $(document).on('click', '.btn-eraser-clear', function() {
            $("canvas").each(function(i) {
                if ($(this).hasClass('active')) {
                    var can = $(this);
                    var ctxcan = can[0].getContext('2d');
                    ctxcan.clearRect(0, 0, can[0].width, can[0].height);

                    //$("#pencils-body li.hr1").addClass('active');
                    $("#erasers-body li").removeClass('active');
                    $('#divrubber').css("display", "none");
                    canDraw();
                } else {
                    $('#divrubber').css("display", "none");
                    $("#erasers-body li").removeClass('active');
                }
            });
        });
    }

    function pad(val) {
        var valString = val + "";
        if (valString.length < 2) {
            return "0" + valString;
        } else {
            return valString;
        }
    }

    function fileOnload(e) {
        var img = $('<img>', {
            src: e.target.result
        });
        var socket = io.connect(url);
        var cnt = getCanvas();
        var can = $('#math' + cnt);
        var ctxcan = can[0].getContext('2d');

        // alert(img.src.value);
        // var canvas1 = $('#paper')[0];
        // var context1 = canvas1.getContext('2d');
        img.on('load', function() {
            ctxcan.drawImage(this, positionx, positiony);
            socket.emit('fileperaltri', {
                'id': id,
                'positionx': positionx,
                'positiony': positiony,
                'fileperaltri': e.target.result,
                'room': stanza
            });
        });
    }

    function canEraser() {
        //username = username.substr(0, 20);
        var socket = io.connect(url);
        var cnt = getCanvas();
        var can = $('#math' + cnt);
        var ctxcan = can[0].getContext('2d');

        can.off();

        divrubber.on('mouseup mouseleave', function(e) {
            drawing = false;
            controlrubber = false;
            dragging = true;
        });

        divrubber.on('mousemove', function(e) {
            var rubbersize = getEraser();

            if (dragging) {
                ctxcan.clearRect(divrubber.position().left, divrubber.position().top, rubbersize + 4, rubbersize + 4);
                controlrubber = true;

                socket.emit('rubber', {
                    'x': divrubber.position().left,
                    'y': divrubber.position().top,
                    'id': id,
                    'usernamerem': username,
                    'controlrubber': controlrubber,
                    'width': rubbersize + 4,
                    'height': rubbersize + 4,
                    'room': stanza
                });
            }
        });

        divrubber.on('mousedown', function(e) {
            drawing = false;
            dragging = true;
        });
    }


    $('.trigger-action').on('click', function () {
           let divws = $(this).parents('.tutor-ws-div');

           if (divws.hasClass('activate')) {
                divws.removeClass('activate');
           } else {
                divws.addClass('activate');
           }
        });

    function canDraw() {
        var socket = io.connect(url);
        var cnt = getCanvas();
        var can = $('#math' + cnt);
        var ctxcan = can[0].getContext('2d');

        var prev = {};

        // ctx setup
        ctxcan.lineCap = "round";
        ctxcan.lineJoin = "round";
        ctxcan.lineWidth = getPencil();
        ctxcan.font = "20px Tahoma";

        if (ctxcan) {
            $(window).on('resize', function () {
                resizecanvas(can, ctxcan);
            });

            $(window).on('orientationchange', function () {
                resizecanvas(can, ctxcan);
            });
        }

        socket.emit('setuproom', {
            'room': stanza,
            'id': username,
            'usernamerem': username
        });

        socket.on('setuproomserKO', function(data) {
            stanza = data.room;
        });

        socket.on('setuproomser', function(data) {
            stanza = data.room;
        });

        socket.on('doppioclickser', function(data) {
            ctxcan.fillStyle = data.color;
            ctxcan.font = data.fontsizerem + "px Tahoma";
            ctxcan.fillText(data.scrivi, data.x, data.y);
        });

        socket.on('fileperaltriser', function(data) {
            var imgdaclient = new Image();
            imgdaclient.src = data.fileperaltri;
            imgdaclient.onload = function() {
                //  imgdaclient.src = data.fileperaltri;
                ctxcan.drawImage(imgdaclient, data.positionx, data.positiony);
            }
        });

        socket.on('moving', function(data) {
            // if (!(data.id in clients)) {
            //     // a new user has come online. create a cursor for them
            //     cursors[data.id] = $('<div class="cursor"><div class="identif">' + data.usernamerem + '</div>').appendTo('#cursors');
            // }

            // Move the mouse pointer
            // cursors[data.id].css({
            //     'left': data.x,
            //     'top': data.y
            // });

            // Is the user drawing?
            if (data.drawing && clients[data.id]) {
                // Draw a line on the canvas. clients[data.id] holds
                // the previous position of this user's mouse pointer
                ctxcan.strokeStyle = data.color;
                //  drawLinerem(clients[data.id].x, clients[data.id].y, data.x, data.y,data.spessremo,data.color);
                drawLinerem(clients[data.id].x, clients[data.id].y, data.x, data.y, data.spessremo, data.color, ctxcan);
            }

            // Saving the current client state
            clients[data.id] = data;
            clients[data.id].updated = $.now();
        });

        socket.on('toccomoving', function(data) {

            // if (!(data.id in clients)) {
            //     // a new user has come online. create a cursor for them
            //     cursors[data.id] = $('<div class="cursor"><div class="identif">' + data.usernamerem + '</div>').appendTo('#cursors');
            // }

            // Move the mouse pointer
            // Is the user drawing?
            if (data.drawing && clients[data.id]) {

                // cursors[data.id].css({
                //     'left': data.x,
                //     'top': data.y
                // });

                // Draw a line on the canvas. clients[data.id] holds
                // the previous position of this user's mouse pointer
                ctx.strokeStyle = data.color;
                //  drawLinerem(clients[data.id].x, clients[data.id].y, data.x, data.y,data.spessremo,data.color);
                drawLinerem(clients[data.id].x, clients[data.id].y, data.x, data.y, data.spessremo, data.color, ctxcan);
            }

            // Saving the current client state
            clients[data.id] = data;
            clients[data.id].updated = $.now();
        });

        socket.on('rubberser', function(data) {

            // if (!(data.id in clients)) {
            //     // a new user has come online. create a cursor for them
            //     cursors[data.id] = $('<div class="cursor"><div class="identif">' + data.usernamerem + '</div>').appendTo('#cursors');
            // }

            // Move the mouse pointer
            // Is the user drawing?
            if (data.controlrubber && clients[data.id]) {

                cursors[data.id].css({
                    'left': data.x,
                    'top': data.y
                });
                ctxcan.clearRect(data.x, data.y, data.width, data.height);
            }

            // Saving the current client state
            clients[data.id] = data;
            clients[data.id].updated = $.now();
        });

        //  code to draw on canvas
        $('#panel').on('touchstart', function(e) {
            e.preventDefault();
            getTouchPos();

            socket.emit('mousemove', {
                'x': touchX,
                'y': touchY,
                'drawing': drawing,
                'color': getColor(),
                'id': id,
                'usernamerem': username,
                'spessremo': getPencil(),
                'room': stanza
            });
            $(".cursor").css("zIndex", 6);
            drawing = true;
        }, false);

        $('#panel').on('touchend', function(e) {
            e.preventDefault();
            drawing = false;
            $(".cursor").css("zIndex", 8);
        }, false);

        $('#panel').on('touchmove', function(e) {
            var cnt = getCanvas();
            var can = $('#math' + cnt);
            var ctxcan = can[0].getContext('2d');

            e.preventDefault();
            if ($.now() - lastEmit > 25) {
                if (controlpencil) {
                    prev.x = touchX;
                    prev.y = touchY;
                    getTouchPos();

                    drawLineMultiCanvas(prev.x, prev.y, touchX, touchY, ctxcan);

                    lastEmit = $.now();
                    socket.emit('mousemove', {
                        'x': touchX,
                        'y': touchY,
                        'drawing': drawing,
                        'color': getColor(),
                        'id': id,
                        'usernamerem': username,
                        'spessremo': getPencil(),
                        'room': stanza
                    });
                }
            }

        }, false);

        $('#panel').on('mousedown', function(e) {
            e.preventDefault();
            prev.x = e.pageX + 5;
            prev.y = e.pageY - 55;
            socket.emit('mousemove', {
                'x': prev.x,
                'y': prev.y,
                'drawing': drawing,
                'color': getColor(),
                'id': id,
                'usernamerem': username,
                'spessremo': getPencil(),
                'room': stanza
            });
            drawing = true;
            $(".cursor").css("zIndex", 6);
        });

        $('#panel').on('mouseup mouseleave', function(e) {
            if (drawing) {
                drawing = false;
                $(".cursor").css("zIndex", 8);
                //history.saveState();

                if (!$('.btn-grid').hasClass('active')) {
                    $state['draw'] = can[0].toDataURL();
                }
            }
        });

        $('#panel').on('mousemove', function(e) {
            var cnt = getCanvas();
            var can = $('#math' + cnt);
            var ctxcan = can[0].getContext('2d');

            var posmousex = e.pageX + 5;
            var posmousey = e.pageY - 65;
            if ($.now() - lastEmit > 25) {
                if (drawing && (controlpencil)) {
                    //     ctx.strokeStyle = document.getElementById('minicolore').value;
                    drawLineMultiCanvas(prev.x, prev.y, e.pageX + 5, e.pageY - 65, ctxcan);
                    prev.x = e.pageX + 5;
                    prev.y = e.pageY - 65;
                    lastEmit = $.now();
                    socket.emit('mousemove', {
                        'x': prev.x,
                        'y': prev.y,
                        'drawing': drawing,
                        'color': getColor(),
                        'id': id,
                        'usernamerem': username,
                        'spessremo': getPencil(),
                        'room': stanza
                    });

                }
            }
            // Draw a line for the current user's movement, as it is
            // not received in the socket.on('moving') event above
        });
    }

    function getCanvas() {
        var cnt = '1';
        $("#layers-body li").each(function(i) {
            if ($(this).hasClass('active')) {
                cnt = $(this).attr('data-cnt');
            }
        });
        return cnt;
    }

    function getColor() {
        var color = '#000000';
        $("#colors-body li").each(function(i) {
            if ($(this).hasClass('active')) {
                color = $(this).attr('data-color');
            }
        });
        return color;
    }

    function getPencil() {
        var pencil = '1';
        $("#pencils-body li").each(function(i) {
            if ($(this).hasClass('active')) {
                pencil = $(this).attr('data-pencil');
            }
        });
        return pencil;
    }

    function getEraser() {
        var eraser = 0;
        $("#erasers-body li").each(function(i) {
            if ($(this).hasClass('active')) {
                eraser = parseInt($(this).attr('data-eraser'));
            }
        });
        return eraser;
    }

    function getBg() {
        var bg = '';
        $("#grids-body .btn-color-grid").each(function(i) {
            if ($(this).hasClass('active')) {
                bg = $(this).attr('data-color');
            }
        });
        return bg;
    }

    function getGrid() {
        var grid = '';
        $("#grids-body .btn-grid").each(function(i) {
            if ($(this).hasClass('active')) {
                grid = parseInt($(this).attr('data-grid'));
            }
        });
        return grid;
    }

    function getTab() {
        var tab = 0;
        $("#tab-chat li").each(function(i) {
            if ($(this).hasClass('active')) {
                tab = parseInt($(this).attr('data-id'));
            }
        });
        return tab;
    }

    function cloneCanvas(index, active = false) {
        //create a new canvas
        var newCanvas = document.createElement('canvas');
        var context = newCanvas.getContext('2d');

        //set dimensions
        newCanvas.width = $('.main-notepad').width();
        newCanvas.height = $('.main-notepad').height();
        newCanvas.id = "math" + index;

        if (active) {
            newCanvas.className = "math-panel active";
            newCanvas.style.visibility = "visible";
        } else {
            newCanvas.className = "math-panel";
            newCanvas.style.visibility = "hidden";
        }


        //return the new canvas
        return newCanvas;
    }

    function drawLinerem(fromx, fromy, tox, toy, spessore, colorem, ctx) {
        ctx.strokeStyle = colorem;
        ctx.lineWidth = spessore;
        ctx.beginPath();
        ctx.moveTo(fromx, fromy);
        ctx.lineTo(tox, toy);
        ctx.stroke();
        fromx = tox;
        fromy = toy;
    }

    function drawLineMultiCanvas(fromx, fromy, tox, toy, ctx) {
        ctx.strokeStyle = getColor();
        ctx.lineWidth = getPencil();
        ctx.beginPath();
        ctx.moveTo(fromx, fromy);
        ctx.lineTo(tox, toy);
        ctx.stroke();
    }

    function resizecanvas(can, ctxcan) {
        var imgdata = ctxcan.getImageData(0, 0, can[0].width, can[0].height);
        can[0].width = window.innerWidth;
        can[0].height = window.innerHeight + 65;
        ctxcan.putImageData(imgdata, 0, 0);
    }

    function getTouchPos(e) {
        if (!e)
            var e = event;

        if (e.touches) {
            if (e.touches.length == 1) { // Only deal with one finger
                var touch = e.touches[0]; // Get the information for finger #1
                // touchX=touch.pageX-touch.target.offsetLeft;
                // touchY=touch.pageY-touch.target.offsetTop;
                touchX = touch.pageX - 17;
                touchY = touch.pageY - 95;
            }
        }
    }

    function activeTab() {
        $(document).on('click', '.item-stt-message', function() {
            var id = $(this).attr('data-id');
            var channels = [];
            $('.item-stt-message').removeClass('active');
            $('.inbox-message').css("display", "none");
            $(this).addClass('active');
            $("#tab-chat li").each(function(i) {
                if (!$(this).hasClass('active')) {
                    var room = $(this).attr('data-room');
                    channels.push('/' + room);
                }
            });
            if (id == 0) {
                getSubscribe(clients, stanza, testichat);
                if (channels.length) unSubscribe(clients, channels);
                $('#testichat').css("display", "block");
            } else {
                var username = $(this).attr('data-name');
                var roomid;
                var stanza = roomid = $(this).attr('data-room');
                var testichats = document.getElementById('testichat' + id);
                getSubscribe(clients, roomid, testichats);
                if (channels.length) unSubscribe(clients, channels);
                $('#testichat' + id).css("display", "block");
            }
        });
    }

    window.addEventListener("load", () => {
        function startPosition(e) {
            painting = true;
            draw(e);
        }

        function finishedPosition() {
            painting = false;
            context.beginPath();
        }

        function draw(e) {
            if (!painting) return;
            context.lineWidth = 1;
            // context.strokeStyle = "red";
            context.lineCap = "round";

            context.lineTo(e.clientX, e.clientY);
            context.stroke();
        }

        if ($('#canvas').length) {
            const canvas = document.querySelector('#canvas');
            const context = canvas.getContext("2d");
            //   resizing
            canvas.height = window.innerHeight;
            canvas.width = window.innerWidth;
            //   variables
            let painting = false;

            //   eventListeners
            canvas.addEventListener('mousedown', startPosition);
            canvas.addEventListener('mouseup', finishedPosition);
            canvas.addEventListener('mouseleave', finishedPosition);
            canvas.addEventListener('mousemove', draw);
        }
    });

})(jQuery, window, document);